class Question
{
public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4); ~Question();
	string* getAnswers();	
	string getQuestion();
	int getCorrectAnswerIndex();
	int getId();
private:
	string _question;int 
	_correctAnswerIndex;
	string _answers[4];
};

//ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

typedef struct RoomData {
	std::string name;
	unsigned int id;
	unsigned int questionsCount;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	bool isActive;
} RoomData;


class Room
{
private:

	RoomData* m_metadata;
	std::vector<User*> _users
public:
	Room(RoomData RD, LoggedUser user);
	void addUser(User);
	void removeUser(User);
	vector<LoggedUser> getAllUsers() const;
	RoomData getRoomData() const;
	void createRoom(User, RoomData);
	void deleteRoom(unsigned int);

};



//ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
class User
{
public:
	User(string, SOCKET);
	User(string, SOCKET, Room*, Game*);
	void send(string);
	void setGame(Game*);
	void clearRoom();
	bool joinRoom(Room*);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	~User();

private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};
//fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

using namespace std;

class Signup
{
public:
	string username;
	string password;


	SignupRequest(string username, string password);
};
//SignupRequest::SignupRequest(string username, string password)
//{
//	this->username = username;
//	this->password = password;
//}


//fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

class Validator
{
public:
	static bool isUsernameValid(string username);
	static bool isPasswordValid(string password);

};

/*
bool Validator::isUsernameValid(string username)
{
	if (username.size() == 0) 
	 return false;

	if (!isalpha(username[0])) 
	 return false;

	for (int i = 0; i < username.size(); i++)
		if (isspace(username[i]))
		 return false;
}
*/
/*
bool Validator::isPasswordValid(string password)
{
	int Counter = 0;
	int Upper = 0;
	int Lower = 0;

	if (password.size() < 4)
		return false;

	for (int i = 0; i < password.size(); i++)
	{
		if (isdigit(password[i])) Counter++;

		else if (islower(password[i])) Lower++;

		else if (isupper(password[i])) Upper++;

		if (isspace(password[i])) return false;
	}

	if (digitCounter && Upper && Lower)
		return true;

	return false;
}
*/